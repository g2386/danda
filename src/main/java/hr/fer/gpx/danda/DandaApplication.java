package hr.fer.gpx.danda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DandaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DandaApplication.class, args);
	}

}
